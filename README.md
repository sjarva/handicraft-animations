# Handicraft themed CSS Animations

I like many types of handicrafts - knitting, sewing, crochet, weaving, tatting... you name it! As an enthusiast, I love also everything miscellaneous that is handicraft themed: t-shirts, mugs, pen cases, notebooks etc. As a web designer and developer I've noticed that not many web pages or services (even if there were handicraft related) style or theme their apps or sites with handicraft items, which is a shame. I felt like there's room for showcasing how we could use handicraft items for example in web page animations.

This project is about creating different animations with handicraft related items and objects (skeins of yarn, loom, spindle, spinning wheel, needles etc) with CSS animations.

## Technologies used

So far, all animations are done using only HTML5 and CSS (which is pre-processed by SCSS).

## Knitting related animations

### Stash Overflow

A storage of yarn is called a stash. When you have too much yarn in the stash, the stash overflows. (Also, this a developer reference to StackOverflow forum and the error state of a stack exceeding its size).

## Spinning

### Drop spindle
One way of spinning yarn is to use a drop spindle, an object that is hanging in the air by the spinned thread.